var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "My First dataset",
            backgroundColor: [
                'rgb(78, 99, 132)',
                'rgb(255, 123, 132)',
                'rgb(65, 99, 132)',
                'rgb(44, 99, 132)',
                'rgb(70, 99, 132)',
                'rgb(55, 99, 42)',
                'rgb(255, 99, 23)'
            ],
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45],
        }]
    },

    // Configuration options go here
    options: {}
});
